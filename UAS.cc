/* Nama Program : Uas.cc
   Tgl buat     : 8 Februari 2024
    Deskripsi    : Jawaban no-2 
    --------------------------------------------------- */
        
    #include <iostream>
    using namespace std;

    int main() {
        double nilai, quiz, absen, uts, uas, tugas;
        double persen_quiz, persen_absen, persen_uts, persen_uas, persen_tugas;
        char Huruf_Mutu;

        cout << "Masukkan nilai Quiz: ";
        cin >> quiz;
        cout << "Masukkan nilai Absen: ";
        cin >> absen;
        cout << "Masukkan nilai UTS: ";
        cin >> uts;
        cout << "Masukkan nilai UAS: ";
        cin >> uas;
        cout << "Masukkan nilai Tugas: ";
        cin >> tugas;

        cout << "Masukkan persentase Quiz (0-100): ";
        cin >> persen_quiz;
        cout << "Masukkan persentase Absen (0-100): ";
        cin >> persen_absen;
        cout << "Masukkan persentase UTS (0-100): ";
        cin >> persen_uts;
        cout << "Masukkan persentase UAS (0-100): ";
        cin >> persen_uas;
        cout << "Masukkan persentase Tugas (0-100): ";
        cin >> persen_tugas;

        nilai = ((persen_absen/100) * absen) + ((persen_tugas/100) * tugas) + ((persen_quiz/100) * quiz) + ((persen_uts/100) * uts) + ((persen_uas/100) * uas);

        if (nilai > 85 && nilai <= 100)
            Huruf_Mutu = 'A';
        else if (nilai > 70 && nilai <= 85)
            Huruf_Mutu = 'B';
        else if (nilai > 55 && nilai <= 70)
            Huruf_Mutu = 'C';
        else if (nilai > 40 && nilai <= 55)
            Huruf_Mutu = 'D';
        else if (nilai >= 0 && nilai <= 40)
            Huruf_Mutu = 'E';

        cout << "Nilai akhir: " << nilai << endl;
        cout << "Huruf Mutu : " << Huruf_Mutu << endl;

        return 0;
    }
